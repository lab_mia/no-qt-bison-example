#include "Command.h"

#include <stdlib.h>

CREATE_DISK *createDisk(int value, UNIT_TYPE unit, char * cadena)
{
    CREATE_DISK *b = (CREATE_DISK *)malloc(sizeof(CREATE_DISK));

    if (b == NULL) /* Ya no hay memoria */
        return NULL;

    b->unit = unit;
    b->value = value;
    b->nombre = cadena;

    return b;
}

void deleteCreateDisk(CREATE_DISK *b)
{
    if (b == NULL)
        return;

    free(b);
}
COMMAND *allocateCOMMAND(){
    COMMAND *c = (COMMAND *)malloc(sizeof(COMMAND));

    if (c == NULL) return NULL;

    c->name = cNONE;
    c->create_disk = NULL;

    return c;
}

void deleteCOMMAND(COMMAND *c){
    if (c == NULL)
        return;
    deleteCreateDisk(c->create_disk);
    free(c);
}
