# Clase2MIA
Breve ejemplo de un analizador léxico y sintáctico usando Flex y Bison sin utilizar QT

## Instalar herramientas

```bash
sudo dnf install flex-devel  # Analizador léxico
sudo dnf install bison-devel # Analizador sintáctico
sudo dnf install gcc-c++     # Compiladores para C/C++
sudo dnf install make        # Build tool

# Puedes instalar todas las herramientas con un solo comando:
sudo dnf install flex-devel bison-devel gcc-c++ make
```
## Compilación

Para compilar el ejemplo, basta con abrir una terminal, navegar al directorio donde
se encuentran los archivos y ejecutar el comando:

```bash
make
```
Para ejecutar la aplicación utiliza el comando:
```bash
./ejemplo
```
Este proyecto utiliza Make para automatizar el proceso de compilación, el archivo nombrado
Makefile le indica a Make las dependencias (en este caso flex y bison) así como de los comandos
que debe ejecutar y en que orden para compilar la aplicación.
