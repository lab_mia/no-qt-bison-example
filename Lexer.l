%{

#include "Command.h"
#include "Parser.h"

#include <stdio.h>
#include <string.h>
%}

%option outfile="Lexer.c" header-file="Lexer.h"
%option warn nodefault

%option reentrant noyywrap never-interactive nounistd
%option bison-bridge

%%

[ \r\n\t]*   { continue; /* Ignoramos espacios. */ }
[0-9]+       { sscanf(yytext, "%d", &yylval->value); return TOKEN_NUMBER; }
"MB"       { yylval->value = tMB; return TOKEN_MB; }
"KB"       { yylval->value = tKB; return TOKEN_KB; }
"GB"       { yylval->value = tGB; return TOKEN_GB; }
"--"          { return DDASH; }
"="          { return IGUAL; }
"create_disk"          { return CMD_create_disk; }
"size"          { return SIZE; }
[a-zA-Z_]+   { yylval->value = strdup(yytext); return TOKEN_CADENA; }
.            { continue; /* Ignore unexpected characters. */}

%%
