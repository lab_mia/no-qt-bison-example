#include "Command.h"
#include "Parser.h"
#include "Lexer.h"

#include <stdio.h>

int yyparse(COMMAND **cmd, yyscan_t scanner);

COMMAND *parse(const char *cmd)
{
    COMMAND *command;
    yyscan_t scanner;
    YY_BUFFER_STATE state;

    if (yylex_init(&scanner)) {
        return NULL;
    }

    state = yy_scan_string(cmd, scanner);

    if (yyparse(&command, scanner)) {
        /* error parsing */
        printf("Error de sintaxis!\n");
        return NULL;
    }

    yy_delete_buffer(state, scanner);

    yylex_destroy(scanner);

    return command;
}

int main(void)
{
    char test[] = "create_disk --size=76GB MIA_ARCHIVOS"; /** Comando de prueba, la palabra MIA_ARCHIVOS deberia ser extraida como un ID **/
    COMMAND *cmd = parse(test);
    switch(cmd->name){
        case cCREATE_DISK: {
            CREATE_DISK *r = cmd->create_disk;
            printf("Crear disco. Tamanio:%d. Unidad: %d. Cadena: %s\n", r->value, r->unit, r->nombre);
        } break;
        default: {
            printf("Comando no reconocido :(");
        }
    }
    deleteCOMMAND(cmd);
    return 0;
}
