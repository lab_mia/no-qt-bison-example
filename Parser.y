%{

#include "Command.h"
#include "Parser.h"
#include "Lexer.h"

int yyerror(COMMAND **cmd, yyscan_t scanner, const char *msg) { /* No hay error handling */ return 0; }

%}

%code requires {
  typedef void* yyscan_t;
}

%output  "Parser.c"
%defines "Parser.h"

%define api.pure
%lex-param   { yyscan_t scanner }
%parse-param { COMMAND **command }
%parse-param { yyscan_t scanner }

%union {
    int value;
    char * cadena;
    COMMAND *command;
    UNIT_TYPE unit;
}

%token DDASH   "--"
%token IGUAL   "="
%token CMD_create_disk     "create_disk"
%token SIZE     "size"
%token <unit> TOKEN_MB    "MB"
%token <unit> TOKEN_KB    "KB"
%token <unit> TOKEN_GB    "GB"
%token <value> TOKEN_NUMBER "number"
%token <cadena> TOKEN_CADENA "cadena"

%type <command> cmd
%type <unit> size_unit

%%

input
    : cmd { *command = $1; }
    ;

cmd
    : CMD_create_disk DDASH SIZE IGUAL TOKEN_NUMBER size_unit TOKEN_CADENA
    { $$ = allocateCOMMAND(); $$->name = cCREATE_DISK; 
      $$->create_disk = createDisk($5, $6, $7); /* El parametro $7 ya es del tipo char * por lo que solo se usa directamente */ }
    ;

size_unit: TOKEN_MB { $$ = $1; } 
         | TOKEN_KB { $$ = $1; }
         | TOKEN_GB { $$ = $1; }
         ;

%%
