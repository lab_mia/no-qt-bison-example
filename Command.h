#ifndef __COMMAND_H__
#define __COMMAND_H__

/*** Constantes ENUM ***/

typedef enum tagUNIT
{
    tMB,
    tKB,
    tGB
} UNIT_TYPE;

typedef enum tagCOMMAND_NAME
{
    cNONE,
    cCREATE_DISK
} COMMAND_NAME;

/*** Create disk ***/
typedef struct tagCREATE_DISK
{
    UNIT_TYPE unit; /* Unidad del tamanio */
    int value; /* Valor del tamanio del disco */
    char * nombre; /* ejemplo de una cadena de texto */
    /* --- Otros parametros importantes para implementar el comando --- */
} CREATE_DISK;

CREATE_DISK *createDisk(int value, UNIT_TYPE unit, char * cadena);
void deleteCreateDisk(CREATE_DISK *b);

/*** Command ***/
typedef struct tagCOMMAND {
    COMMAND_NAME name;
    CREATE_DISK *create_disk;
    /* Otros commandos a implementar */
} COMMAND;

COMMAND *allocateCOMMAND();
void deleteCOMMAND(COMMAND *b);

#endif /* __COMMAND_H__ */
