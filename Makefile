# Makefile

FILES = Lexer.c Parser.c Command.c main.c
CC = cc 
CFLAGS = -g -ansi

main: $(FILES)
	$(CC) $(CFLAGS) $(FILES) -o ejemplo

Lexer.c: Lexer.l
	flex Lexer.l

Parser.c: Parser.y Lexer.c
	bison Parser.y

clean:
	rm -f *.o *~ Lexer.c Lexer.h Parser.c Parser.h ejemplo
