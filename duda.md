# Bison - Error al retornar una cadena de texto extraída de un símbolo terminal - MIA - Proyecto 1

Tu implementación de Bison es correcta, el problema es con tu implementación en Flex.
Sin embargo, cambiaría la declaración de ```char TEXT[256]``` por ```char * cadena```
la razón es que al alocar un arreglo de chars de 256 posiciones estás desperdiciando espacio en 
memoria porque no sabés realmente de que tamaño va a ser la cadena y sin embargo ya alocaste todo el 
espacio por adelantado. Por eso, cuando trabajas con cadenas de tamaño variable en C, se recomienda 
alocarlas como char * a menos que sepás exactamente cuantos caracteres va a tener o si vas a escribir
el struct en disco.

Aparte de eso, debes revisar tu código en Flex. Mi ejemplo no incluía la obtención de una cadena de
texto, así que lo agregué. Los cambios importantes son:

## Lexer.l
```
[a-zA-Z_]+   { yylval->value = strdup(yytext); return TOKEN_CADENA; }
```

A la izquierda está una regex simple para un identificador, a la derecha la producción en Flex.
Si te dás cuenta la variable global ```yylval-value``` Es la variable que devuelve el valor del símbolo
terminal, mientras que el return devuelve únicamente una constante que identifica al token dentro del parser.
Esto significa que el return value de una producción en Flex es únicamente un identificador para uso interno
del parser, pero la asignación de la variable ```yylval->value``` es la que devuelve el verdadero valor que
se va a obtener del terminal en Bison.

## Parser.y
```yacc

%union {
    int value;
    char * cadena; /* Se agrega el nuevo tipo cadena */
    COMMAND *command;
    UNIT_TYPE unit;
}
 /* ---- otras partes del archivo ---- */

%token <cadena> TOKEN_CADENA "cadena" /* Se declara al TOKEN_CADENA que devuelve Flex y se le asigna el tipo definido anteriormente */

 /* ---- Continúa el archivo ---- */

cmd
    : CMD_create_disk DDASH SIZE IGUAL TOKEN_NUMBER size_unit TOKEN_CADENA
    { $$ = allocateCOMMAND(); $$->name = cCREATE_DISK; 
      $$->create_disk = createDisk($5, $6, $7); /* El parametro $7 ya es del tipo char *, por lo que solo se usa directo */ }
    ;

```

## Command.h
```c
typedef struct tagCREATE_DISK
{
    UNIT_TYPE unit; 
    int value; 
    char * nombre; /* Se agrega un nuevo campo de tipo cadena para ilustrar la extracción exitosa de la cadena del parser. */
} CREATE_DISK;


CREATE_DISK *createDisk(int value, UNIT_TYPE unit, char * cadena); /* Se modifica la signature de la función constructor para aceptar el nuevo argumento */

```

## Command.c
```c
CREATE_DISK *createDisk(int value, UNIT_TYPE unit, char * cadena)
{
    CREATE_DISK *b = (CREATE_DISK *)malloc(sizeof(CREATE_DISK));

    if (b == NULL) /* Ya no hay memoria */
        return NULL;

    b->unit = unit;
    b->value = value;
    b->nombre = cadena; /* Implementación del nuevo constructor que acepta el nuevo parametro del tipo cadena */

    return b;
}
```

## main.c
```c
    char test[] = "create_disk --size=76GB MIA_ARCHIVOS"; /** Comando de prueba, la palabra MIA_ARCHIVOS debe ser reconocida como ID y extraída **/
    COMMAND *cmd = parse(test);
    switch(cmd->name){
        case cCREATE_DISK: {
            CREATE_DISK *r = cmd->create_disk;
            printf("Crear disco. Tamanio:%d. Unidad: %d. Cadena: %s\n", r->value, r->unit, r->nombre); /* Se impreme la cadena obtenida para verificar su correcta extracción */
        } break;
        default: {
            printf("Comando no reconocido :(");
        }
    }
```

Como se puede observar en el resumen, los cambios son casi mínimos. El repositorio ya se encuentra actualizado con el código completo.
Los errores más comunes son con el uso de Flex, ya que la documentación oficial no es muy clara en como retornar valores arbitrarios
con cada token. Lo más importante es verificar que valor se le está asignando a la variable ```yylval->value``` Ya que el valor que se 
le asigne será el devuelto por el terminal cuando se consuma desde Bison.
